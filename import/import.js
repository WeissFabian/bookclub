var fs = require('fs');
var CsvReadableStream = require('csv-reader');

//Make DB ready to be written to 
var pg = require("pg");
const conString = process.env.DB_CON_STRING;
pg.defaults.ssl = true;
var dbClient = new pg.Client(conString);
dbClient.connect();

 
var inputStream = fs.createReadStream('books.csv', 'utf8');
 
inputStream
    .pipe(CsvReadableStream({ parseNumbers: true, parseBooleans: true, trim: true }))
    .on('data', function (row) {
        dbClient.query("INSERT INTO books (isbn, title, author, release) VALUES ($1, $2, $3, $4)", [row[0], row[1], row[2], row[3]], function(dbError, dbResponse){
            console.log("Sucessfully loaded");
        })
        
    })
    .on('end', function (data) {
        console.log('No more rows!');
    });