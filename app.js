var express = require("express");
var pg = require("pg");
var bodyParser = require("body-parser");
var session = require("express-session");

//to encrypt the passwords 
const bcrypt = require('bcrypt');
const saltRounds = 10;


//to be able to use google books
var books = require('google-books-search');

var CON_STRING = process.env.DB_CON_STRING;
if (CON_STRING == undefined) {
    console.log("Error: Environment variable DB_CON_STRING not set!");
    process.exit(1);
}

pg.defaults.ssl = true;
var dbClient = new pg.Client(CON_STRING);
dbClient.connect();

var urlencodedParser = bodyParser.urlencoded({
    extended: false
});

const PORT = 3000;

var app = express();
//required to use css files
app.use(express.static("public"));

app.use(session({
    secret: "This is a secret!",
    resave:true,
    saveUninitialized: false
}));

app.set("views", "views");
app.set("view engine", "pug");

app.get("/", function (req, res) {
    res.redirect("login");
});

app.get("/login", function (req, res) {
    res.render("login", {username: req.session.user});
});

app.get("/error", function (req, res) {
    res.render("error", {username: req.session.user});
});

app.get("/impressum", function (req, res) {
    res.render("impressum", {username: req.session.user});
});

//login und registrieren
app.post("/login", urlencodedParser, function (req, res) {
    var inputValue = req.body.cnct_btn;
    var username = req.body.username;
    var password = req.body.password;

    if(username.length == 0 || password.length == 0){
        res.render("login", {login_error: "Bitte geben sie einen Benutzernamen und Passwort ein"});
        return;
    }

//schauen welchen button der user gedrückt hat ob er eingloggt oder registriert werden will
    if(inputValue == "register"){
        
        //schauen das noch kein so ein user da is mit dem namen
        dbClient.query("SELECT * FROM users WHERE username=$1", [username], function (dbError, dbResponse) {
            if (dbResponse.rows == 0) {
        //wenn keien gleicher vorhanden neu anlegen

                //encrypt password then store it
                bcrypt.hash(password, saltRounds, function(err, hash) {
                    dbClient.query("INSERT INTO users (username, password) VALUES ($1, $2)", [username, hash], function(dbError, dbResponse){})
                });
                
                req.session.user = username;
                //console.log("registered and loged in");
                res.redirect("login");
        //sonst fehler ausgeben das schon vorhanden
            } else {
                res.render("login", {login_error: "Dieser Username ist bereits vergeben"});
            }
          });

    }else if(inputValue == "login"){

        //schauen ob username und passwort zusammen passen
        dbClient.query("SELECT * FROM users WHERE username=$1", [username], function (dbError, dbResponse) {
            if (dbResponse.rows == 0) {
        //wenn es ned passt auf login bleiben und user mitteilen das es ned passt
              res.render("login", {login_error: "User ist nicht registriert"});
        
        //Passwort und username haben gepasst      
            } else {
                
                bcrypt.compare(password, dbResponse.rows[0].password, function(err, response) {
                    if(response == true){
                        req.session.user = username;
                        //console.log("loged in");
                        res.redirect("search");
                    }else{
                        res.render("login", {login_error: "User und Passwort stimmen nicht überein"});
                    }
                });
                 
            }
          });
    }else{
        //console.log("FEHLER AUFGETRETEN BEI DEN LOGIN/REGISTER BUTTONS!");
        res.redirect("login", {login_error: "FEHLER AUFGETRETEN BEI DEN LOGIN/REGISTER BUTTONS!"});
    }
});


//ausloggen und sessions beenden
app.get("/logout", function (req, res) {

    req.session.destroy(function (err) {
        //console.log("Session destroyed.");
    });
    res.redirect("login");
});


//wennn zugriff auf search dann nur eingeloggte user rein lassen 
app.get("/search", function(req, res){
    if (isLogedIn(req.session.user)) {
        res.render("search", {username: req.session.user, books: []});
    } else {
        res.redirect("login");
    }
});


//such seite
app.post("/search", urlencodedParser, function (req, res) {

    if (!isLogedIn(req.session.user)) {
        res.redirect("login");
        return;
    }

//eingaben holen
    var isbn = req.body.isbnInput;
    var title = req.body.titleInput;
    var author = req.body.authorInput;

//schauen ob alle Felder leer sind
    if(isbn.length == 0 && title.length == 0 && author.length == 0){
        res.render("search", {username: req.session.user, books: [], error: "Bitte eine Eingabe tätigen"});
        return;
    }

    
//prüfen ob befüllt wenn ned auf leeren wert setzen
    if(isbn.length == 0){
        isbn = "";
        //console.log("isbn set to stern");
    }
    if(title.length == 0){
        title = "";
    }
    if(author.length == 0){
        author = "";
    }

//setzen für Teil string suche
    isbn = "%" + isbn + "%";
    title = "%" + title + "%";
    author = "%" + author + "%";

//alle einträge für die eingabe holen
    dbClient.query("SELECT * FROM books WHERE isbn LIKE $1 AND title LIKE $2 AND author LIKE $3", [isbn, title, author], function (dbError, dbResponse){
   
        //wenn kein buch vorhanden dann das ausgeben
        if(dbResponse.rows == 0){
            res.render("search", {username: req.session.user, books: [], error: "Kein Buch vorhanden"});
            //console.log("hier");
        //wenn bücher vorhanden dann an pug geben und ausgeben
        }else{

           
            res.render("search", {username: req.session.user, amount: dbResponse.rows.length, books: dbResponse.rows});
            
            //console.log("woanders");
        }
        
    });
});








//aufruf der einzelbuecher info
app.get("/books/:id", function (req, res) {
    if (!isLogedIn(req.session.user)) {
        res.redirect("../login");
        return;
    }
    
    //get books infos 
    var book_id = req.params.id;
    dbClient.query("SELECT * FROM books WHERE id=$1", [book_id], function (dbError, dbBookResponse) {
        //wenn das book ned da dann error
        if (dbBookResponse.rows.length == 0) {
            res.render("error", {username: req.session.user,
                error: "Dieses Buch ist nicht vorhanden"
            });
        } else {
        //sonst bucher info ausgeben
            //die reviews holen
            dbClient.query("SELECT * FROM reviews WHERE book_id=$1", [book_id], function (dbError, dbRevResponse) {
                books.search(dbBookResponse.rows[0].title, function(error, results) {
                    if ( ! error ) {
                        //console.log(results[0].thumbnail);
                    } else {
                        //console.log(error);
                    }
                

                    if(dbRevResponse.rows == 0){
                        res.render("books", {username: req.session.user, piclink: results[0].thumbnail, book: dbBookResponse.rows[0]});
                    }else{

                    
                    dbClient.query("SELECT AVG(ranking) FROM reviews WHERE book_id=$1;", [book_id], function (dbError, dbStrResponse){
            
                        
                        if(dbStrResponse.rows == 0){
                            res.render("books", {username: req.session.user, piclink: results[0].thumbnail, book: dbBookResponse.rows[0], reviews: dbRevResponse.rows})
                        
                        }else{
                            res.render("books", {username: req.session.user, piclink: results[0].thumbnail, book: dbBookResponse.rows[0], reviews: dbRevResponse.rows, avrgStrs: Number(Math.round(dbStrResponse.rows[0].avg+'e2')+'e-2')})
                        }
                    });
                
                    }
                });
            });
        }
    });
});




//review eintrag
app.post("/books/:id", urlencodedParser, function (req, res) {
    //check if user is loged in
    if (!isLogedIn(req.session.user)) {
        res.redirect("../login");
        return;
    }

    //book id holen
        var book_id = req.params.id;

    //input holen
        var revtext  = req.body.revtextInput;
        var ranking = req.body.selectpicker;

    //die id vom eingeloggten user holen
        var user_id;
        var username = req.session.user;

    dbClient.query("SELECT * FROM users WHERE username=$1", [username], function (dbError, dbResponse){
        user_id = dbResponse.rows[0].id;
        

    // check if book_id da 
    dbClient.query("SELECT * FROM books WHERE id=$1", [book_id], function (dbError, dbBookResponse) {
    //wenn weg dann error
        if (dbBookResponse.rows.length == 0) {
            res.render("error", { username: req.session.user, error: "Dieses Buch ist nicht vorhanden"});
        }
    });

//warum muss ich des rein tun draußen erkennt er user_id ned 
            //schauen ob der user schon eine bewertung hat
            dbClient.query("SELECT * FROM reviews WHERE user_id=$1 AND book_id=$2", [user_id, book_id], function (dbError, dbRevResponse){
                
                //wenn kein review da is darf eins rein
                if(dbRevResponse.rows == 0){
                    //checken ob eingabe wert ned 1-5 is 
                    if(ranking > 5 || ranking < 1){
                        booksError("Bitte eine Bewertung zwischen 1-5", book_id, res, req);
                        return;
                    }
                    //sonst einfach hinzufügen udn seite neu laden
                    dbClient.query("INSERT INTO reviews (revtext, ranking, book_id, user_id) VALUES ($1, $2, $3, $4)", [revtext, ranking, book_id, user_id], function(dbError, dbResponse){
                        res.redirect("/books/"+book_id);
                    })
                }else{
                    //wenn schon eins da dann einfach error auf der books seite ausgeben
                    booksError("Sie haben bereits bewertet", book_id, res, req);
                    return;
                }
            });
    });

});



//falls ein fehler aufgetreten is boocks seite mit error ausgeben 
function booksError(errorMsg, book_id, res, req){
    dbClient.query("SELECT * FROM books WHERE id=$1", [book_id], function (dbError, dbBookResponse) {
                            

            //die reviews holen
            dbClient.query("SELECT * FROM reviews WHERE book_id=$1", [book_id], function (dbError, dbRevResponse) {

                dbClient.query("SELECT AVG(ranking) FROM reviews WHERE book_id=$1;", [book_id], function (dbError, dbStrResponse){
                    
                    //des thumbnail vom buch holen
                    books.search(dbBookResponse.rows[0].title, function(error, results) {
                        if ( ! error ) {
                            //console.log(results[0].thumbnail);
                        } else {
                            //console.log(error);
                        }
                    
                        //books seite mit den jeweiligen infos des buchs laden und den übergebenen error ausgeben
                        if (dbRevResponse.rows.length == 0) {
                            res.render("books", {username: req.session.user, piclink: results[0].thumbnail, book: dbBookResponse.rows[0], error: errorMsg});
                        } else {
                            res.render("books", {username: req.session.user, piclink: results[0].thumbnail, book: dbBookResponse.rows[0], reviews: dbRevResponse.rows, avrgStrs: Number(Math.round(dbStrResponse.rows[0].avg+'e2')+'e-2'), error: errorMsg});
                        }
                    });
                });
            });
    });
    return;
}



//aufruf der einzelbuecher info jahr
app.get("/release/:year", function (req, res) {
    if (!isLogedIn(req.session.user)) {
        res.redirect("../login");
        return;
    }
    
    //get books infos 
    var year = req.params.year;
    //alle einträge für die eingabe holen
    dbClient.query("SELECT * FROM books WHERE release=$1", [year], function (dbError, dbResponse){
   
        //wenn kein buch vorhanden dann das ausgeben
        if(dbResponse.rows == 0){
            res.render("detailSingle", {username: req.session.user, books: [], error: "Kein Buch vorhanden"});
        //wenn bücher vorhanden dann an pug geben und ausgeben
        }else{
            res.render("detailSingle", {username: req.session.user, books: dbResponse.rows, detail: "Jahr: " + year});
        }
        
    });
});


//aufruf der einzelbuecher info autor
app.get("/author/:name", function (req, res) {
    if (!isLogedIn(req.session.user)) {
        res.redirect("../login");
        return;
    }
    
    //get books infos 
    var name = req.params.name;
    //console.log(name);
    //alle einträge für die eingabe holen
    dbClient.query("SELECT * FROM books WHERE author=$1", [name], function (dbError, dbResponse){
   
        //wenn kein buch vorhanden dann das ausgeben
        if(dbResponse.rows == 0){
            res.render("detailSingle", {username: req.session.user, books: [], error: "Kein Buch vorhanden"});
        //wenn bücher vorhanden dann an pug geben und ausgeben
        }else{
            res.render("detailSingle", {username: req.session.user, books: dbResponse.rows, detail: name});
        }
        
    });
    
});







function isLogedIn(user) {
    if(user == undefined){
        return false;
    }
    return true;
    
  }




app.listen(PORT, function () {
    console.log(`Book App listening on Port ${PORT}`);
});
